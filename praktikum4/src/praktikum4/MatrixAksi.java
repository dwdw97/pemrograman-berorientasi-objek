package praktikum4;

public class MatrixAksi {
	public static void main(String[] args) {
		double[][] a = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9} };
        double[][] d = { { 1, 2, 3 }, { 4, 5, 6 }, { 9, 1, 3} };
        
        Matrix A = new Matrix(a);
        Matrix B = A.transpose();
        Matrix C = Matrix.identity(3);
        Matrix D = new Matrix(d);
        
        // Matrix A
        System.out.println("Matriks A:");
        A.show();
        System.out.println("\nMatriks B:");
        B.show();
        System.out.println();
        System.out.println("Matriks C:");
        C.show();
        System.out.println("\nMatriks D:");
        D.show();
        System.out.println("\nMatriks A + Matriks B:");
        A.plus(B).show();
        System.out.println("\nMatriks B x Matriks A:");
        B.times(A).show();
        System.out.println("\nMatrix A (swapped):");
        A.swap(1, 2);
        A.show();
        System.out.println();
        // shouldn't be equal since AB != BA in general   
        System.out.println(A.times(B).eq(B.times(A)));
        System.out.println();
        Matrix b = Matrix.random(5, 1);
        b.show();
        System.out.println();
        Matrix x = A.solve(b);
        x.show();
        System.out.println();
        A.times(x).show();
    }
}
