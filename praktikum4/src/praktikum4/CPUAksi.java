package praktikum4;

public class CPUAksi {
	public static void main(String[] args) {
		CPU myCPU = new CPU();
		CPU.Processor myProcessor = myCPU.new Processor();
		CPU.RAM myRAM = myCPU.new RAM();
		
		System.out.println("Processor Cache = "+myProcessor.getCache());
		System.out.println("Ram Clock speed = "+myRAM.getClockSpeed());
	}
}
