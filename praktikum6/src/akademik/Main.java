package akademik;
import mahasiswa.Mahasiswa;

public class Main {

	public static void main(String[] args) {
		Mahasiswa aku = new Mahasiswa("A11.2020.13208", "Dicky Wisnu Dwiwahyudi", 3.55, 24, "1997-04-12");
		System.out.println("Program Studi         : "+aku.getProgdi());
		System.out.println("Status IPK            : "+aku.ipkStatus());
		System.out.println("Angkatan              : "+aku.getTahun());
		System.out.println("Tagihan               : Rp"+aku.getTagihanSks());
		System.out.println("Semester diselesaikan : "+aku.getMhsSemester());
		System.out.println("Umur                  : "+aku.getUmur()+" tahun");
	}
}