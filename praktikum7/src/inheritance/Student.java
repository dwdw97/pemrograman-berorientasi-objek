package inheritance;

public class Student extends Person {
	String nim;
	int spp, sks, modul;
	
	public String getNim() {
		return nim;
	}
	
	@Override
	public void identity() {
		System.out.println("NIM              : "+getNim());
		super.identity();
		System.out.println("Total Pembayaran : Rp"+hitungPembayaran());
	}
	
	public String hitungPembayaran() {
		int total = spp+(sks*250000)+modul;
		return InheritMain.currencyFormatter(total);
	}
}