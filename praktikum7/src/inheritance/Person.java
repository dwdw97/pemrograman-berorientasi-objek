package inheritance;

public class Person {
	protected String name, address, hobi;
	
	public void identity() {
		System.out.println("Nama             : "+name);
		System.out.println("Alamat           : "+address);
		System.out.println("Hobi             : "+hobi);
	}
}
