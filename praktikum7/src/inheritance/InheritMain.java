package inheritance;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Scanner;

public class InheritMain {
	public static void main(String[] args) {
		Scanner inp = new Scanner(System.in);
		Student mahasiswa = new Student();
		
		System.out.print("NIM: ");
		mahasiswa.nim = inp.nextLine();
		System.out.print("Nama: ");
		mahasiswa.name = inp.nextLine();
		System.out.print("Alamat: ");
		mahasiswa.address = inp.nextLine();
		System.out.print("Nominal SPP: ");
		mahasiswa.spp = inp.nextInt();
		System.out.print("Jumlah SKS: ");
		mahasiswa.sks = inp.nextInt();
		System.out.print("Harga Modul: ");
		mahasiswa.modul = inp.nextInt();
		System.out.println("--------------");
		mahasiswa.identity();
	}
	
	static protected String currencyFormatter(int currency) {
		String pattern = "###,###.00";
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator(',');
		symbols.setGroupingSeparator('.');
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		
		return decimalFormat.format(currency);
	}
}