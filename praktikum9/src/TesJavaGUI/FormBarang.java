package TesJavaGUI;

import java.awt.EventQueue;
import java.sql.*;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager; 
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;


public class FormBarang {

	private JFrame frmFormBarang;
	private JTextField txtKdBrg;
	private JTextField txtNmBrg;
	private JTextField txtStok;
	private JTextField txtStokMin;
	private JComboBox boxSatuan;
	
	// java-mysql connect
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://127.0.0.1/penjualan";
	static final String USER = "root";
	static final String PASS = "";
	
	static Connection conn;
	static Statement stmt;
	static ResultSet rs;
	private JTable tableBrg;
	private DefaultTableModel model;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FormBarang window = new FormBarang();
					window.frmFormBarang.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FormBarang() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmFormBarang = new JFrame();
		frmFormBarang.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				showData();
			}
		});
		frmFormBarang.setTitle("Form Barang");
		frmFormBarang.setBounds(100, 100, 460, 481);
		frmFormBarang.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmFormBarang.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Data Barang");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setBounds(24, 28, 120, 25);
		frmFormBarang.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Kode barang");
		lblNewLabel_1.setBounds(24, 75, 83, 14);
		frmFormBarang.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Nama barang");
		lblNewLabel_1_1.setBounds(24, 100, 83, 14);
		frmFormBarang.getContentPane().add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_2 = new JLabel("Satuan");
		lblNewLabel_1_2.setBounds(24, 125, 83, 14);
		frmFormBarang.getContentPane().add(lblNewLabel_1_2);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Stok");
		lblNewLabel_1_1_1.setBounds(24, 150, 83, 14);
		frmFormBarang.getContentPane().add(lblNewLabel_1_1_1);
		
		JLabel lblNewLabel_1_3 = new JLabel("Stok Minimal");
		lblNewLabel_1_3.setBounds(24, 175, 83, 14);
		frmFormBarang.getContentPane().add(lblNewLabel_1_3);
		
		txtKdBrg = new JTextField();
		txtKdBrg.setBounds(117, 72, 79, 20);
		frmFormBarang.getContentPane().add(txtKdBrg);
		txtKdBrg.setColumns(10);
		
		txtNmBrg = new JTextField();
		txtNmBrg.setColumns(10);
		txtNmBrg.setBounds(117, 97, 109, 20);
		frmFormBarang.getContentPane().add(txtNmBrg);
		
		txtStok = new JTextField();
		txtStok.setColumns(10);
		txtStok.setBounds(117, 147, 79, 20);
		frmFormBarang.getContentPane().add(txtStok);
		
		txtStokMin = new JTextField();
		txtStokMin.setColumns(10);
		txtStokMin.setBounds(117, 172, 79, 20);
		frmFormBarang.getContentPane().add(txtStokMin);
		
		boxSatuan = new JComboBox();
		boxSatuan.setModel(new DefaultComboBoxModel(new String[] {"Buah", "Lembar", "Liter", "Kilogram", "Batang"}));
		boxSatuan.setBounds(117, 121, 79, 22);
		frmFormBarang.getContentPane().add(boxSatuan);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 261, 424, 170);
		frmFormBarang.getContentPane().add(scrollPane);
		
		JButton btnSimpan = new JButton("Simpan");
		btnSimpan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String kode = txtKdBrg.getText();
				String nama = txtNmBrg.getText();
				Object satuan = boxSatuan.getSelectedItem();
				int stok = Integer.parseInt(txtStok.getText());
				int stokmin = Integer.parseInt(txtStokMin.getText());
				
				insert(kode, nama, satuan, stok, stokmin);
			}
		});
		btnSimpan.setBounds(24, 215, 89, 23);
		frmFormBarang.getContentPane().add(btnSimpan);
		
		tableBrg = new JTable();
		tableBrg.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String kd_brg = tableBrg.getValueAt(tableBrg.getSelectedRow(), 0).toString();
				getData(kd_brg);
				btnSimpan.setEnabled(false);
			}
		});
		scrollPane.setViewportView(tableBrg);
		
		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ubahData(txtKdBrg.getText());
			}
		});
		btnEdit.setBounds(123, 215, 89, 23);
		frmFormBarang.getContentPane().add(btnEdit);
		
		JButton btnHapus = new JButton("Hapus");
		btnHapus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int response = JOptionPane.showConfirmDialog(null, "Yakin ingin menghapus?");
				if (response==0) {
					if(tableBrg.getSelectedRow()>0) {
						hapusData(txtKdBrg.getText());
					}
				} else {
					JOptionPane.showMessageDialog(null, "Hapus data dibatalkan");
				}
			}
		});
		btnHapus.setBounds(222, 215, 89, 23);
		frmFormBarang.getContentPane().add(btnHapus);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtKdBrg.setText("");
				txtNmBrg.setText("");
				boxSatuan.setSelectedIndex(-1);
				txtStok.setText("");
				txtStokMin.setText("");
				btnSimpan.setEnabled(true);
			}
		});
		btnReset.setBounds(321, 215, 89, 23);
		frmFormBarang.getContentPane().add(btnReset);
		
		JMenuBar menuBar = new JMenuBar();
		frmFormBarang.setJMenuBar(menuBar);
		
		JMenu mnDataMaster = new JMenu("Data Master");
		menuBar.add(mnDataMaster);
		
		JMenu mnTransaksi = new JMenu("Transaksi");
		menuBar.add(mnTransaksi);
		
		JMenu mnLaporan = new JMenu("Laporan");
		menuBar.add(mnLaporan);
		
		JMenuItem mntmDataBarang = new JMenuItem("Data Barang");
		mntmDataBarang.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName(JDBC_DRIVER);
					conn = DriverManager.getConnection(DB_URL, USER, PASS);
					String sql = "SELECT * FROM barang";

					JasperDesign jdesign = JRXmlLoader.load("E:\\dwdw\\Programming\\Java\\TesJavaGUI\\res\\report.jrxml");

					JRDesignQuery updateQuery = new JRDesignQuery();
					updateQuery.setText(sql);
					jdesign.setQuery(updateQuery);

					JasperReport Jreport = JasperCompileManager.compileReport(jdesign);
					JasperPrint JasperPrint = JasperFillManager.fillReport(Jreport, null, conn);

					JasperViewer.viewReport(JasperPrint, false);
					
					ExportToExcel excel = new ExportToExcel(tableBrg, new File("DataPenjualan.xls"));
				} 
				catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		mnLaporan.add(mntmDataBarang);
		
		JMenuItem mntmDataKonsumen = new JMenuItem("Data Konsumen");
		mnLaporan.add(mntmDataKonsumen);
		
		JSeparator separator = new JSeparator();
		mnLaporan.add(separator);
		
		JMenuItem mntmDataPenjualan = new JMenuItem("Data Penjualan");
		mnLaporan.add(mntmDataPenjualan);
		
		JMenu mnUtility = new JMenu("Utility");
		menuBar.add(mnUtility);
	}
	
	public void insert(String kode, String nama, Object satuan, int stok, int stokmin) {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			
			String query = "INSERT INTO barang (kd_brg, nm_brg, satuan, stok_brg, stok_min) VALUES (?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setString(1, kode);
			ps.setString(2, nama);
			ps.setObject(3, satuan);
			ps.setInt(4, stok);
			ps.setInt(5, stokmin);
			
			ps.execute();
			
			stmt.close();
			conn.close();			
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
		
		showData();
	}
	
	public void showData() {
		model = new DefaultTableModel();
		
		model.addColumn("Kode Barang");
		model.addColumn("Nama Barang");
		model.addColumn("Satuan");
		model.addColumn("Stok Barang");
		model.addColumn("Stok Minimal Barang");
		
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery("SELECT * FROM barang");
			while(rs.next()) {
				model.addRow(new Object[] {
					rs.getString("kd_brg"),
					rs.getString("nm_brg"),
					rs.getString("satuan"),
					rs.getString("stok_brg"),
					rs.getString("stok_min"),
				});
			}
			
			stmt.close();
			conn.close();
			
			tableBrg.setModel(model);
			tableBrg.setAutoResizeMode(1);
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
	}
	
	public void getData(String kode) {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			
			String query = "SELECT * FROM barang WHERE kd_brg = ?";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setString(1, kode);
			
			rs = ps.executeQuery();
			
			rs.next();
			
			txtKdBrg.setText(rs.getString("kd_brg"));
			txtNmBrg.setText(rs.getString("nm_brg"));
			boxSatuan.setSelectedItem(rs.getString("satuan"));
			txtStok.setText(rs.getString("stok_brg"));
			txtStokMin.setText(rs.getString("stok_min"));
			
			stmt.close();
			conn.close();
			
			tableBrg.setModel(model);
			tableBrg.setAutoResizeMode(1);
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
	}
	
	public void hapusData(String kode) {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			
			String query = "DELETE FROM barang WHERE kd_brg = ?";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setString(1, kode);
			
			ps.execute();
			
			stmt.close();
			conn.close();
			
			tableBrg.setModel(model);
			tableBrg.setAutoResizeMode(1);
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
		showData();
	}
	
	public void ubahData(String kode) {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			
			String query = "UPDATE barang SET nm_brg = ?, satuan = ?, stok_brg = ?, stok_min = ? WHERE kd_brg=?";
			PreparedStatement ps = conn.prepareStatement(query);
			
			ps.setString(1, txtNmBrg.getText());
			ps.setObject(2, boxSatuan.getSelectedItem());
			ps.setInt(3, Integer.parseInt(txtStok.getText()));
			ps.setInt(4, Integer.parseInt(txtStokMin.getText()));
			ps.setString(5, kode);
			
			ps.execute();
			
			stmt.close();
			conn.close();			
		} catch(Exception e) {
			System.out.println("Koneksi gagal");
			System.out.println(e);
		}
		
		showData();
	}
}
